import logging
from typing import Dict, cast

import uvicorn
import sentry_sdk

from fastapi import FastAPI
from strawberry.fastapi import GraphQLRouter
import strawberry
from strawberry.types import Info

from src.clients import PairClient, PokemonClient, StarwarsClient
from src.domain import PairUps
from src.models import Pair, ReadOnlyResource
from src.utils import config, setup_logger


@strawberry.experimental.pydantic.type(model=ReadOnlyResource, all_fields=True)
class ReadOnlyResourceType:
    pass


@strawberry.experimental.pydantic.type(model=Pair, all_fields=True)
class PairType:
    pass


@strawberry.type
class Query:
    @strawberry.field
    async def team(self, info: Info) -> PairType:  # type: ignore
        pair = await info.context["pair_ups"].get(1)
        return cast(PairType, pair)


@strawberry.type
class Mutation:
    @strawberry.mutation
    async def create_team(
        self, pokemon_id: str, starwars_id: str, info: Info  # type: ignore
    ) -> PairType:
        pair = await info.context["pair_ups"].create(pokemon_id, starwars_id)
        return cast(PairType, pair)


schema = strawberry.Schema(query=Query, mutation=Mutation)


async def get_context() -> Dict[str, PairUps]:
    pair_ups = PairUps()
    await pair_ups.init(
        pair_client=PairClient(),
        pokemon_client=PokemonClient(),
        starwars_client=StarwarsClient(),
    )
    return {"pair_ups": pair_ups}


graphql_app = GraphQLRouter(schema, context_getter=get_context)  # type: ignore
app = FastAPI()
app.include_router(graphql_app, prefix="/graphql")
if __name__ == "__main__":
    uvicorn.run(
        "src.gql:app",
        host=config.host,
        port=config.port,
        log_level="error",
        reload=True,
    )

# sentry_sdk.init(config.sentry_url)
setup_logger(config)
logging.info(f"Running on {config.host}:{config.port}")
