from typing import List

import pytest

from src.domain import PairUps
from src.models import Pair, ClientBase, ReadOnlyResource, ReadOnlyClientBase

STARWARS_NAME = "Luke Skywalker"
POKEMON_NAME = "Bulbasaur"
ID = "1"


class MockClient(ClientBase):
    def __init__(self) -> None:
        self.pairs: List[Pair] = []

    async def init(self) -> None:
        pass

    async def get(self, pair_id: int) -> Pair:
        return next(pair for pair in self.pairs if pair.id == pair_id)

    async def create(self, pair: Pair) -> Pair:
        pair.id = 1
        self.pairs.append(pair)
        return pair


class MockReadOnlyClient(ReadOnlyClientBase):
    def __init__(self) -> None:
        self.resources: List[ReadOnlyResource] = []

    async def get(self, resource_id: str) -> ReadOnlyResource:
        return next(
            resource for resource in self.resources if resource.id == resource_id
        )

    async def create(self, resource: ReadOnlyResource) -> ReadOnlyResource:
        resource.id = ID
        self.resources.append(resource)
        return resource


expected = Pair(
    id=1,
    name=f"{STARWARS_NAME} and {POKEMON_NAME}!",
    pokemon=ReadOnlyResource(id=ID, name=POKEMON_NAME),
    starwars_char=ReadOnlyResource(id=ID, name=STARWARS_NAME),
)


def describe_pair_ups() -> None:
    pair_ups = PairUps()

    @pytest.fixture
    async def before() -> None:
        pair_client = MockClient()
        pokemon_client = MockReadOnlyClient()
        starwars_client = MockReadOnlyClient()
        await pair_ups.init(
            pair_client=pair_client,
            pokemon_client=pokemon_client,
            starwars_client=starwars_client,
        )
        await starwars_client.create(ReadOnlyResource(id=ID, name=STARWARS_NAME))
        await pokemon_client.create(ReadOnlyResource(id=ID, name=POKEMON_NAME))

    @pytest.fixture
    async def pair() -> Pair:
        return await pair_ups.create(pokemon_id=ID, starwars_id=ID)

    @pytest.mark.asyncio
    async def it_creates_a_pair(
        before: None,  # pylint: disable=unused-argument
    ) -> None:
        actual = await pair_ups.create(pokemon_id=ID, starwars_id=ID)
        assert actual == expected

    @pytest.mark.asyncio
    async def it_gets_a_pair(pair: Pair) -> None:
        assert pair.id
        actual = await pair_ups.get(pair.id)
        assert actual == expected
