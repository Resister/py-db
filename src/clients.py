from typing import cast

import requests
from sqlmodel import SQLModel, select
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

from src.entities import PairEntity, PokemonEntity, StarwarsEntity
from src.models import Pair, ClientBase, ReadOnlyResource, ReadOnlyClientBase

from src.utils import GQLClient


class PairClient(ClientBase):
    async def init(self) -> None:
        self.engine = create_async_engine(
            "sqlite+aiosqlite:///database.sqlite", future=True
        )
        async with self.engine.begin() as conn:
            await conn.run_sync(SQLModel.metadata.create_all)

    async def create(self, pair: Pair) -> Pair:
        name = pair.name
        pokemon_id = pair.pokemon.id
        pokemon_name = pair.pokemon.name
        starwars_id = pair.starwars_char.id
        starwars_name = pair.starwars_char.name
        pair_entity = PairEntity(
            name=name,
            pokemon_id=pokemon_id,
            starwars_id=starwars_id,
        )

        pokemon_entity = PokemonEntity(id=pokemon_id, name=pokemon_name)
        starwars_entity = StarwarsEntity(id=starwars_id, name=starwars_name)
        async with AsyncSession(self.engine) as session:
            session.add(pair_entity)
            if not await session.get(PokemonEntity, pokemon_id):
                session.add(pokemon_entity)

            if not await session.get(StarwarsEntity, starwars_id):
                session.add(starwars_entity)
            await session.commit()
            await session.refresh(pair_entity)
            pair.id = pair_entity.id
            return pair

    async def get(self, pair_id: int) -> Pair:
        async with AsyncSession(self.engine) as session:
            statement = (
                select(PairEntity, PokemonEntity, StarwarsEntity)
                .join(PokemonEntity)
                .join(StarwarsEntity)
                .where(PairEntity.id == pair_id)
            )
            results = await session.execute(statement)
            for pair, pokemon, starwars in results:
                pair_model = Pair(
                    id=pair.id,
                    name=pair.name,
                    pokemon=ReadOnlyResource(id=pokemon.id, name=pokemon.name),
                    starwars_char=ReadOnlyResource(id=starwars.id, name=pokemon.name),
                )
            return pair_model


class PokemonClient(ReadOnlyClientBase):
    def __init__(self) -> None:
        self.gql_client = GQLClient(url="https://graphql-pokemon2.vercel.app")

    async def get(self, resource_id: str) -> ReadOnlyResource:
        query = f'{{pokemon(id:"{resource_id}"){{id name}}}}'
        pokemon = await self.gql_client.exec(query, ReadOnlyResource)
        return cast(ReadOnlyResource, pokemon)


class StarwarsClient(ReadOnlyClientBase):
    def __init__(self) -> None:
        self.url = "https://swapi-api.hbtn.io/api/people"

    async def get(self, resource_id: str) -> ReadOnlyResource:
        response = requests.get(f"{self.url}/{resource_id}/")
        return ReadOnlyResource(id=resource_id, **response.json())
