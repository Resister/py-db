import asyncio

from src.domain import PairUps
from src.clients import PairClient, PokemonClient, StarwarsClient

pair_ups = PairUps()


async def run() -> None:
    await pair_ups.init(
        pair_client=PairClient(),
        pokemon_client=PokemonClient(),
        starwars_client=StarwarsClient(),
    )

    await pair_ups.create(pokemon_id="UG9rZW1vbjowMDE=", starwars_id="1")
    res = await pair_ups.get(1)
    print(res.dict())


asyncio.run(run())
