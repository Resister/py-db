from typing import TypeVar, Union, Callable, List, Type, Optional, Literal, cast
import re
import logging
from os import environ
import distutils.util

import requests
from pydantic import BaseModel, HttpUrl


T = TypeVar("T")  # pylint: disable=invalid-name


class GQLClient:
    def __init__(self, url: str):
        self.url = url

    async def exec(
        self, query: str, resource_class: Callable[..., T]
    ) -> Union[List[T], T]:
        match = re.search(r"(?<={)\w+", query.replace(" ", ""))
        if not match:
            raise Exception("Malformed Query")
        response = requests.post(self.url, json={"query": query})
        json = response.json()
        data = json["data"][match.group()]
        if isinstance(data, list):
            return [resource_class(**datum) for datum in data]
        return resource_class(**data)


Parseable = Union[str, bool, int]


def _parse(key: str, type_arg: Type[Parseable]) -> Parseable:
    if type_arg == int:
        return int(key)
    if type_arg == bool:
        return bool(distutils.util.strtobool(key))
    if type_arg == str:
        return key
    raise ValueError("Can only parse boolean, number and strings")


def get_var(
    key: str,
    default: Optional[Parseable] = None,
    type_arg: Optional[Type[Parseable]] = None,
) -> Parseable:
    res = environ.get(key)
    if res and default:
        return _parse(res, type(default))
    if res and type_arg:
        return _parse(res, type_arg)
    if not res and default:
        return default
    if not default or not type_arg:
        raise ValueError("Either type_arg or default required")
    raise ValueError(f"Environment Variable {key} Required")


LogLevel = Literal["ERROR", "WARN", "INFO", "DEBUG"]


class Config(BaseModel):
    log_level: LogLevel
    sentry_url: HttpUrl
    port: int
    host: str
    e2e: bool


config = Config(
    sentry_url=cast(HttpUrl, get_var("SENTRY_URL", type_arg=str)),
    log_level=cast(LogLevel, get_var("LOG_LEVEL", default="DEBUG")),
    port=cast(int, get_var("PORT", default=3000)),
    host=cast(str, get_var("HOST", default="127.0.0.1")),
    e2e=cast(bool, get_var("E2E", default="0")),
)


def setup_logger(cfg: Config) -> None:
    level = cfg.log_level.upper()
    logging.basicConfig(format="%(levelname)s: %(message)s", level=level)
