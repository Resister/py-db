from abc import ABC, abstractmethod
from typing import Optional
from pydantic import BaseModel


class ReadOnlyResource(BaseModel):
    id: str
    name: str


class Pair(BaseModel):
    id: Optional[int]
    name: str
    pokemon: ReadOnlyResource
    starwars_char: ReadOnlyResource


class ReadOnlyClientBase(ABC):
    @abstractmethod
    async def get(self, resource_id: str) -> ReadOnlyResource:
        pass


class ClientBase(ABC):
    @abstractmethod
    async def init(self) -> None:
        pass

    @abstractmethod
    async def get(self, pair_id: int) -> Pair:
        pass

    @abstractmethod
    async def create(self, pair: Pair) -> Pair:
        pass
