from typing import TypeVar

from src.models import Pair, ClientBase, ReadOnlyClientBase


R_con = TypeVar(  # pylint: disable=invalid-name
    "R_con", contravariant=True, bound=ReadOnlyClientBase
)
C_con = TypeVar(  # pylint: disable=invalid-name
    "C_con", contravariant=True, bound=ClientBase
)


class PairUps:
    async def init(
        self,
        pair_client: C_con,
        pokemon_client: R_con,
        starwars_client: R_con,
    ) -> None:
        self.pair_client = pair_client
        self.pokemon_client = pokemon_client
        self.starwars_client = starwars_client
        await self.pair_client.init()

    async def create(self, pokemon_id: str, starwars_id: str) -> Pair:
        pokemon = await self.pokemon_client.get(pokemon_id)
        starwars_char = await self.starwars_client.get(starwars_id)
        name = f"{starwars_char.name} and {pokemon.name}!"
        return await self.pair_client.create(
            Pair(name=name, pokemon=pokemon, starwars_char=starwars_char)
        )

    async def get(self, pair_id: int) -> Pair:
        return await self.pair_client.get(pair_id)
