from typing import Optional

from sqlmodel import SQLModel, Field


class PokemonEntity(SQLModel, table=True):
    id: Optional[str] = Field(default=None, primary_key=True)
    name: str


class StarwarsEntity(SQLModel, table=True):
    id: Optional[str] = Field(default=None, primary_key=True)
    name: str


class PairEntity(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    pokemon_id: str = Field(default=None, foreign_key="pokemonentity.id")
    starwars_id: str = Field(default=None, foreign_key="starwarsentity.id")
