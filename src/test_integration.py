from fastapi.testclient import TestClient
import pook
from src.utils import config

from src.gql import app


PIKA_ID = "UG9rZW1vbjowMjU="

client = TestClient(app)

STARWARS_NAME = "Luke Skywalker"
POKEMON_NAME = "Pikachu"
if not config.e2e:
    STARWARS_NAME = "Joe"
    POKEMON_NAME = "MockPokemon"
    pook.post(
        "https://graphql-pokemon2.vercel.app",
        reply=200,
        response_type="json",
        # json body must match exactly
        json={"query": f'{{pokemon(id:"{PIKA_ID}"){{id name}}}}'},
        response_json={"data": {"pokemon": {"name": POKEMON_NAME, "id": PIKA_ID}}},
        activate=True,
    )
    pook.get(
        "https://swapi-api.hbtn.io/api/people/1/",
        reply=200,
        response_type="json",
        response_json={"name": STARWARS_NAME},
    )


def test_create_team() -> None:
    query = (
        f'mutation {{createTeam(pokemonId: "{PIKA_ID}", starwarsId: "1"){{ name id }}}}'
    )
    response = client.post("/graphql", json={"query": query}).json()
    assert "errors" not in response
    assert (
        response["data"]["createTeam"]["name"] == f"{STARWARS_NAME} and {POKEMON_NAME}!"
    )
